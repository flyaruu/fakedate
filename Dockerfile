FROM rust:1-alpine

MAINTAINER Christian Mauduit "ufoot@ufoot.org"
LABEL package="fakedate" version="0.1.2" description="fakedate generates pseudo-random dates and times."

ADD ./ /wk/
WORKDIR /wk/
RUN cargo build --release
RUN cp /wk/target/release/fakedate /

USER nobody
ENTRYPOINT ["/fakedate"]
